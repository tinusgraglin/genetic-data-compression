This directory includes external libraries in foreign languages:

1. For suffix array computing:
   - [libsais](https://github.com/IlyaGrebnov/libsais) in C under Apache License 2.0.