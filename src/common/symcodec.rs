use std::io::{Read, Write, Error as IOError, Seek};
pub trait SymbolCodec {
    fn encode<R, W>(
        &self, 
        i_byte_reader: &mut R,
        o_byte_writer: &mut W, 
        symbol_read_limit: u32
    ) -> Result<u32, IOError>
    where 
        R: Read, 
        W: Write + Seek
   ;
    
    fn decode<R, W>(
        i_byte_reader: &mut R,
        o_byte_writer: &mut W,
    ) -> Result<u32, IOError>
    where
        R: Read + Seek,
        W: Write
   ;
}