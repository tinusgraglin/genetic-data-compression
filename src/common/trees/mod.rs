pub mod measurable_tree;

pub enum TrivialNodeFullBinTree<T> {
    Node {
        lc: Box<TrivialNodeFullBinTree<T>>,
        rc: Box<TrivialNodeFullBinTree<T>>,
    },
    Leave(T),
}