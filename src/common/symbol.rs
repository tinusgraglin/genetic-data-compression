#[derive(Debug)]
pub struct SymWithFreqU32<S> {
    pub sym: S,
    pub freq: u32,
}

pub trait SymbolWithin8Bits {
    fn bits_needed() -> u8;
    fn from_u8(val: u8) -> Self;
    fn val_in_u8(&self) -> u8;
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct TribitSymbol {
    val: u8
}

impl SymbolWithin8Bits for TribitSymbol {
    #[inline(always)]
    fn bits_needed() -> u8 {
        3
    }

    #[inline(always)]
    fn from_u8(val: u8) -> Self {
       TribitSymbol { val } 
    }

    #[inline(always)]
    fn val_in_u8(&self) -> u8 {
        self.val
    }
}

impl SymbolWithin8Bits for u8 {
    #[inline(always)]
    fn bits_needed() -> u8 {
        8
    }

    #[inline(always)]
    fn from_u8(val: u8) -> Self {
        val
    }

    #[inline(always)]
    fn val_in_u8(&self) -> u8 {
        *self
    }
}