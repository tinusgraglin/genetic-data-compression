#[derive(Debug, Clone)]
pub struct VarLenBitCode<T> {
    pub len: u8,
    pub code: T
}

pub type VarLenBitCode32 = VarLenBitCode<u32>;
