use core::slice;
use std::{io::{BufReader, Read}, fs::File};
use common::symbol::SymWithFreqU32;

#[allow(dead_code, unused)]
mod algo;
#[allow(dead_code, unused)]
mod common;
#[allow(dead_code, unused)]
#[cfg(test)]
mod test;

fn onebyte_symbol_freq_count<R: Read>(reader: &mut R, filter_out_zero: bool) -> Vec<SymWithFreqU32<u8>> {
    let mut table = [0u32; 256];
    let mut b = 0u8;

    while reader.read(slice::from_mut(&mut b)).unwrap() == 1 {
        table[b as usize] += 1;
    }

    let mut v = Vec::new();
    for (i, e) in table.iter().enumerate() {
        if !filter_out_zero || *e > 0 {
            v.push(SymWithFreqU32 { sym: i as u8, freq: *e });
        }
    }
    v
    // let d = vec![
    //     SymWithFreqU32 {sym: 0x41u8, freq: 31311141}, 
    //     SymWithFreqU32 {sym: 0x43u8, freq: 30811210}, 
    //     SymWithFreqU32 {sym: 0x47u8, freq: 30947241},
    //     SymWithFreqU32 {sym: 0x4Eu8, freq: 25},
    //     SymWithFreqU32 {sym: 0x54u8, freq: 30718951}, 
    //     SymWithFreqU32 {sym: 0x0Au8, freq: 49131}
    // ];
    // let d_full = vec![
    //     SymWithFreqU32 {sym: 65u8, freq: 31311141u32}, 
    //     SymWithFreqU32 {sym: 71, freq: 30947241}, 
    //     SymWithFreqU32 {sym: 67, freq: 30860341}, 
    //     SymWithFreqU32 {sym: 84, freq: 30718951}, 
    //     SymWithFreqU32 {sym: 48, freq: 713456}, 
    //     SymWithFreqU32 {sym: 49, freq: 600269}, 
    //     SymWithFreqU32 {sym: 50, freq: 434548}, 
    //     SymWithFreqU32 {sym: 95, freq: 294786}, 
    //     SymWithFreqU32 {sym: 115, freq: 245655}, 
    //     SymWithFreqU32 {sym: 54, freq: 219961}, 
    //     SymWithFreqU32 {sym: 51, freq: 210450}, 
    //     SymWithFreqU32 {sym: 56, freq: 171066}, 
    //     SymWithFreqU32 {sym: 53, freq: 162997}, 
    //     SymWithFreqU32 {sym: 99, freq: 147393}, 
    //     SymWithFreqU32 {sym: 55, freq: 131947}, 
    //     SymWithFreqU32 {sym: 10, freq: 98262}, 
    //     SymWithFreqU32 {sym: 47, freq: 98262}, 
    //     SymWithFreqU32 {sym: 110, freq: 98262}, 
    //     SymWithFreqU32 {sym: 52, freq: 95510}, 
    //     SymWithFreqU32 {sym: 57, freq: 80704}, 
    //     SymWithFreqU32 {sym: 62, freq: 49131}, 
    //     SymWithFreqU32 {sym: 101, freq: 49131}, 
    //     SymWithFreqU32 {sym: 109, freq: 49131}, 
    //     SymWithFreqU32 {sym: 111, freq: 49131}, 
    //     SymWithFreqU32 {sym: 112, freq: 49131}, 
    //     SymWithFreqU32 {sym: 117, freq: 49131}, 
    //     SymWithFreqU32 {sym: 78, freq: 25}
    // ];
}

fn main() {
    let mut reader = BufReader::with_capacity(128 * 1024, File::open("test-files/dataset.fasta").unwrap());
    let v = onebyte_symbol_freq_count(&mut reader, true);
    println!("v: {:?}", v);
}