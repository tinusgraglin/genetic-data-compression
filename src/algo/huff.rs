use std::{
    collections::BinaryHeap,
    io::{Error as IOError, Read, Write, Seek, SeekFrom, ErrorKind}, 
    cmp::Reverse
};

use crate::common::{
    symcodec::SymbolCodec,
    bitcodes::VarLenBitCode32, 
    symbol::{SymbolWithin8Bits, SymWithFreqU32},
    bitio::{BitsRead, BitsWrite},
    trees::{TrivialNodeFullBinTree, measurable_tree::{BottomUpMeasurableFullBiTree as Tree, Measure}}
};

impl<S> Measure for SymWithFreqU32<S> {
    #[inline(always)]
    fn measure_as_u32(&self) -> u32 {
        self.freq
    }
}

pub struct HuffmanCodec<S> {
    tree: Tree<SymWithFreqU32<S>>,
    encode_table_arr: Box<[VarLenBitCode32]>
}

impl<S: SymbolWithin8Bits> HuffmanCodec<S> {
    pub fn build_from(symbol_freq_data: Vec<SymWithFreqU32<S>>, skip_zero_freq: bool) -> Self {
        let mut heap = BinaryHeap::new();
        let mut encode_table_arr = vec![VarLenBitCode32 {len: 0, code: 0}; 1 << S::bits_needed()];

        for SymWithFreqU32 { sym, freq } in symbol_freq_data {
            if !skip_zero_freq || freq > 0 {
                heap.push(Reverse(Tree::Leave(SymWithFreqU32 { sym, freq })));
            }
        }
        while heap.len() > 1 {
            let l = heap.pop().unwrap().0;
            let r = heap.pop().unwrap().0;
            heap.push(Reverse(l + r));
        }
        let tree = heap.pop().unwrap().0;

        fn encode_down_rec<S: SymbolWithin8Bits>(
            tree: &Tree<SymWithFreqU32<S>>,
            depth: u8,
            code: u32,
            table_arr: &mut Vec<VarLenBitCode32>
        ) {
            match tree {
                Tree::Leave(SymWithFreqU32 { sym, .. }) => {
                    table_arr[sym.val_in_u8() as usize] = VarLenBitCode32 {len: depth, code};
                }
                Tree::Node { lc, rc, .. } => {
                    encode_down_rec(lc, depth + 1, code << 1, table_arr);
                    encode_down_rec(rc, depth + 1, (code << 1) | 1, table_arr);
                }
            }
        }

        encode_down_rec(&tree, 0, 0,  &mut encode_table_arr);

        Self {
            tree,
            encode_table_arr: encode_table_arr.into_boxed_slice()
        }        
    }
}

impl<S: SymbolWithin8Bits> SymbolCodec for HuffmanCodec<S> {
    fn encode<R, W>(
        &self, 
        i_byte_reader: &mut R,
        o_byte_writer: &mut W, 
        symbol_read_limit: u32
    ) -> Result<u32, IOError>
    where
        R: Read, 
        W: Write + Seek
    {
        // encoded file structure:
        // |symbol len in bits|tree data len in bits|payload size in bits|tree data|payload data|
        // 0                  1                     3                   11         ?            EOF

        let nbits = S::bits_needed();

        o_byte_writer.write(&[nbits])?;
        o_byte_writer.write(&[0u8;10])?;

        let mut bitreader = BitsRead::create_with(i_byte_reader);
        let mut bitwriter = BitsWrite::create_with(o_byte_writer);

        /// tree data: prefix traversal, output bit 0 for node, 
        /// output bit 1 and data associated (symbol) for leave
        fn write_tree_struct_rec<S: SymbolWithin8Bits, W:Write>(
            tree: &Tree<SymWithFreqU32<S>>, 
            bitwriter: &mut BitsWrite<W>
        ) -> Result<u16, IOError> {
            match tree {
                Tree::Node { lc, rc, .. } =>  {
                    bitwriter.write_bits_32_8_unchecked(0, 1)?;
                    let ln = write_tree_struct_rec(lc, bitwriter)?;
                    let rn = write_tree_struct_rec(rc, bitwriter)?;
                    Ok(ln + rn + 1)
                }
                Tree::Leave(SymWithFreqU32 { sym, .. }) => {
                    bitwriter.write_bits_32_8_unchecked(1, 1)?;
                    bitwriter.write_bits_32_8_unchecked(sym.val_in_u8() as u32, S::bits_needed())?;
                    Ok(1 + S::bits_needed() as u16)
                }
            }
        }
        let tree_struct_bits = write_tree_struct_rec(&self.tree, &mut bitwriter)?;
        bitwriter.flush_buffered_bits()?;

        let mut passed = 0u32;
        let mut payload_bits = 0u64;
        let mut b = 0u8;
        while passed < symbol_read_limit && bitreader.read_bits_8_8_unchecked(&mut b, nbits)? {
            let VarLenBitCode32 {len, code} = self.encode_table_arr[b as usize];

            bitwriter.write_bits_32_8_unchecked(code, len)?;

            payload_bits += len as u64;

            passed += 1;
        }
        bitwriter.flush_buffered_bits()?;

        o_byte_writer.seek(SeekFrom::Start(1))?;
        o_byte_writer.write(&tree_struct_bits.to_le_bytes())?;
        o_byte_writer.write(&payload_bits.to_le_bytes())?;

        Ok(passed)
    }

    fn decode<R, W>(
        i_byte_reader: &mut R,
        o_byte_writer: &mut W,
    ) -> Result<u32, IOError>
    where
        R: Read + Seek,
        W: Write
    {
        let mut onebyte_buf = [0u8];
        let mut twobyte_buf = [0u8;2];
        let mut eightbyte_buf = [0u8;8];
        i_byte_reader.read_exact(&mut onebyte_buf)?;
        i_byte_reader.read_exact(&mut twobyte_buf)?;
        i_byte_reader.read_exact(&mut eightbyte_buf)?;

        let symbol_bits = onebyte_buf[0];
        let treestruct_bits = u16::from_le_bytes(twobyte_buf);
        let payload_bits = u64::from_le_bytes(eightbyte_buf);
        
        let mut bitsreader = BitsRead::create_with(i_byte_reader);

        fn build_decode_tree_rec<R: Read>(
            bitsreader: &mut BitsRead<R>, symbol_bits: u8
        ) -> Result<(u16, TrivialNodeFullBinTree<u8>), IOError> {
            let mut b = 0u8;
            bitsreader.read_bits_8_8_unchecked(&mut b, 1)?;
            if b == 0 {
                let l = build_decode_tree_rec(bitsreader, symbol_bits)?;
                let r = build_decode_tree_rec(bitsreader, symbol_bits)?;
                Ok((
                    1 + l.0 + r.0, 
                    TrivialNodeFullBinTree::Node {
                        lc: Box::new(l.1), 
                        rc: Box::new(r.1)
                    }
                ))
            } else {
                bitsreader.read_bits_8_8_unchecked(&mut b, symbol_bits)?;
                Ok((
                    1 + symbol_bits as u16, 
                    TrivialNodeFullBinTree::Leave(b)
                ))
            }
        }
        let (tree_bits_read, ref ref_root) = build_decode_tree_rec(&mut bitsreader, symbol_bits)?;

        // integrity check #1
        if tree_bits_read != treestruct_bits {
            return Err(
                IOError::new(
                    ErrorKind::InvalidData,
                    format!("Incorrect or corrupted data! (Mismatched tree data bits: expected {} got {})", 
                        treestruct_bits, tree_bits_read)
                )
            )
        }
        
        let mut bitswriter = BitsWrite::create_with(o_byte_writer);
        let mut ref_tree = ref_root;
        let mut passed = 0u32;
        let mut payload_bits_read = 0u64;
        let mut bbuf = [0u8];
        let mut byte = 0u8;
        /* note: initialized to 0u8 to bootstrap the first byte read */
        let mut check_mask = 0u8;

        while payload_bits_read < payload_bits {
            if check_mask == 0u8 {
                if i_byte_reader.read(&mut bbuf)? < 1 {
                    break;
                }
                byte = bbuf[0];
                check_mask = 0x80u8;
            }
            if let TrivialNodeFullBinTree::Node { lc, rc } = ref_tree {
                ref_tree = if byte & check_mask != 0 { rc } else { lc };
                if let TrivialNodeFullBinTree::Leave (data) = ref_tree {
                    if symbol_bits == 8 {
                        bbuf[0] = *data;
                        bitswriter.write_bytes(&bbuf)?;
                    } else {
                        bitswriter.write_bits_32_8_unchecked(*data as u32, symbol_bits)?;
                    }
                    passed += 1;
                    ref_tree = ref_root;
                }
            } else {
                panic!("Unexpected state: should be a non-leave node!");
            }
            check_mask >>= 1;
            payload_bits_read += 1;
        }

        // integrity check #2
        if payload_bits_read < payload_bits || ref_root as *const _ !=  ref_tree as *const _ {
            return Err(
                IOError::new(
                    ErrorKind::InvalidData,
                    format!("Incorrect or corrupted data!")
                )
            )
        }

        Ok(passed)
    }
}
