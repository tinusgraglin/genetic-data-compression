pub mod huff;
pub mod lzw;
pub mod rle;
pub mod fano;
pub mod bwt;