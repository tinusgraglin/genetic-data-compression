# Introduction

This repository is for my genetic data compression class project, for which I need to implement and benchmark some of the data compression techniques ofter used in genetic data compression:   

1. Huffman encoding
2. Fano encoding
3. Lempel-Ziv-Welch encoding
4. Run length encoding
5. Burrows-Wheeler transform

I implemented all of them in naive form in Rust, with the help of [libsais](https://github.com/IlyaGrebnov/libsais) in C for efficiently computing suffix array needed in BWT implementation.